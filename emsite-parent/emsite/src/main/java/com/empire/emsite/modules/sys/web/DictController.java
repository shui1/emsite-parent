/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.sys.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.empire.emsite.common.config.MainConfManager;
import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.utils.CacheUtils;
import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.sys.entity.Dict;
import com.empire.emsite.modules.sys.facade.DictFacadeService;
import com.empire.emsite.modules.sys.utils.DictUtils;
import com.empire.emsite.modules.sys.utils.UserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 类DictController.java的实现描述：字典Controller
 * 
 * @author arron 2017年10月30日 下午7:21:54
 */
@Controller
@RequestMapping(value = "${adminPath}/sys/dict")
public class DictController extends BaseController {

    @Autowired
    private DictFacadeService dictFacadeService;

    @ModelAttribute
    public Dict get(@RequestParam(required = false) String id) {
        if (StringUtils.isNotBlank(id)) {
            return dictFacadeService.get(id);
        } else {
            return new Dict();
        }
    }

    @RequiresPermissions("sys:dict:view")
    @RequestMapping(value = { "list", "" })
    public String list(Dict dict, HttpServletRequest request, HttpServletResponse response, Model model) {
        List<String> typeList = dictFacadeService.findTypeList();
        model.addAttribute("typeList", typeList);
        Page<Dict> page = dictFacadeService.findPage(new Page<Dict>(request, response), dict);
        model.addAttribute("page", page);
        return "modules/sys/dictList";
    }

    @RequiresPermissions("sys:dict:view")
    @RequestMapping(value = "form")
    public String form(Dict dict, Model model) {
        model.addAttribute("dict", dict);
        return "modules/sys/dictForm";
    }

    @RequiresPermissions("sys:dict:edit")
    @RequestMapping(value = "save")
    //@Valid 
    public String save(Dict dict, Model model, RedirectAttributes redirectAttributes) {
        if (MainConfManager.isDemoMode()) {
            addMessage(redirectAttributes, "演示模式，不允许操作！");
            return "redirect:" + adminPath + "/sys/dict/?repage&type=" + dict.getType();
        }
        if (!beanValidator(model, dict)) {
            return form(dict, model);
        }
        dict.setCurrentUser(UserUtils.getUser());
        dictFacadeService.save(dict);
        CacheUtils.remove(DictUtils.CACHE_DICT_MAP);
        addMessage(redirectAttributes, "保存字典'" + dict.getLabel() + "'成功");
        return "redirect:" + adminPath + "/sys/dict/?repage&type=" + dict.getType();
    }

    @RequiresPermissions("sys:dict:edit")
    @RequestMapping(value = "delete")
    public String delete(Dict dict, RedirectAttributes redirectAttributes) {
        if (MainConfManager.isDemoMode()) {
            addMessage(redirectAttributes, "演示模式，不允许操作！");
            return "redirect:" + adminPath + "/sys/dict/?repage";
        }
        dictFacadeService.delete(dict);
        CacheUtils.remove(DictUtils.CACHE_DICT_MAP);
        addMessage(redirectAttributes, "删除字典成功");
        return "redirect:" + adminPath + "/sys/dict/?repage&type=" + dict.getType();
    }

    @RequiresPermissions("user")
    @ResponseBody
    @RequestMapping(value = "treeData")
    public List<Map<String, Object>> treeData(@RequestParam(required = false) String type, HttpServletResponse response) {
        List<Map<String, Object>> mapList = Lists.newArrayList();
        Dict dict = new Dict();
        dict.setType(type);
        List<Dict> list = dictFacadeService.findList(dict);
        for (int i = 0; i < list.size(); i++) {
            Dict e = list.get(i);
            Map<String, Object> map = Maps.newHashMap();
            map.put("id", e.getId());
            map.put("pId", e.getParentId());
            map.put("name", StringUtils.replace(e.getLabel(), " ", ""));
            mapList.add(map);
        }
        return mapList;
    }

    @ResponseBody
    @RequestMapping(value = "listData")
    public List<Dict> listData(@RequestParam(required = false) String type) {
        Dict dict = new Dict();
        dict.setType(type);
        return dictFacadeService.findList(dict);
    }

}
