/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.common.persistence;

/**
 * 类BaseDao.java的实现描述：DAO支持类实现
 * 
 * @author arron 2017年10月30日 下午3:51:51
 */
public interface BaseDao {

}
